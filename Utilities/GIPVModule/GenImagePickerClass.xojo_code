#tag Class
Protected Class GenImagePickerClass
Inherits iOSCanvas
	#tag Event
		Sub Paint(g As iOSGraphics)
		  If photoParDefaut Then
		    photoSelectionne = PhotoPlus
		    g.DrawImage(PhotoSelectionne,0,0)
		  Else
		    g.DrawImage(PhotoSelectionne,0,0,PhotoSelectionne.Width / 50, PhotoSelectionne.height / 50)
		  End If
		  
		  // La Première fois que cette méthode est appelée, premiereFois est à faux
		  //If Not premiereFois Then
		  //premiereFois = True
		  //photoParDefaut = True
		  //photoSelectionne = PhotoPlus
		  //g.DrawImage(PhotoSelectionne,0,0)
		  //ElseIf photoParDefaut Then
		  //photoSelectionne = PhotoPlus
		  //g.DrawImage(PhotoSelectionne,0,0)
		  //Else
		  //g.DrawImage(PhotoSelectionne,0,0,PhotoSelectionne.Width / 50, PhotoSelectionne.height / 50)
		  //End If
		End Sub
	#tag EndEvent

	#tag Event
		Sub PointerDown(pos As Xojo.Core.Point, eventInfo As iOSEventInfo)
		  
		  photoSelection
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From iOSCanvas
		  // Constructor() -- From iOSControl
		  // Constructor(deserializer As xojo.Core._Deserializer) -- From iOSControl
		  Super.Constructor
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub genImagePickerClasssauveImage()
		  RaiseEvent sauvegardeImage
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub photoSelection()
		  Declare Function superview Lib "uikit" selector "superview" (classRef As Ptr) As ptr
		  photoMessageBox = Nil
		  //Dim parentPtr As Ptr = superview(Self.Handle)
		  //Dim parentView As iOSView = IOSView(parentPtr)
		  photoMessageBox = New photoMessageBoxClass(Self)
		  
		  photoMessageBox.Title = ""
		  photoMessageBox.Message = "Sélectionner une option"
		  
		  Dim buttons() As Text
		  If photoParDefaut Then // Photo par défaut 
		    buttons.Append("Annuler")
		    buttons.Append("Prendre une photo")
		    buttons.Append("Choisir une photo existante")
		  Else // Une photo différente de la photo par défaut 
		    buttons.Append("Annuler")
		    buttons.Append("Prendre une photo")
		    buttons.Append("Choisir une photo existante")
		    buttons.Append("Supprimer la photo")
		  End If
		  
		  photoMessageBox.Buttons = buttons
		  
		  photoMessageBox.Show
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event sauvegardeImage()
	#tag EndHook


	#tag Property, Flags = &h0
		photoMessageBox As PhotoMessageBoxclass
	#tag EndProperty

	#tag Property, Flags = &h0
		photoParDefaut As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		photoSelectionne As IOSImage
	#tag EndProperty

	#tag Property, Flags = &h0
		premiereFois As Boolean = True
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AccessibilityHint"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AccessibilityLabel"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="200"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="photoParDefaut"
			Group="Behavior"
			InitialValue="PhotoPlus"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="photoSelectionne"
			Group="Behavior"
			Type="IOSImage"
		#tag EndViewProperty
		#tag ViewProperty
			Name="premiereFois"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="200"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
