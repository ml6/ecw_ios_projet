#tag Class
Protected Class PhotoPicturePickerClass
Inherits iOSPicturePicker
	#tag Event
		Sub Cancelled()
		  //If they cancel, we don't need to change anything
		End Sub
	#tag EndEvent

	#tag Event
		Sub Selected(pic as iOSImage)
		  photoPicturePickerParentPointeur.photoParDefaut = False
		  photoPicturePickerParentPointeur.photoSelectionne = pic
		  photoPicturePickerParentPointeur.genImagePickerClassSauveImage
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Constructor(PhotoMessageBoxParentPtr As GenImagePickerClass)
		  // Calling the overridden superclass constructor.
		  Super.Constructor
		  photoPicturePickerParentPointeur = photoMessageBoxParentPtr
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private photoPicturePickerParentPointeur As GenImagePickerClass
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Editable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Source"
			Group="Behavior"
			Type="Sources"
			EditorType="Enum"
			#tag EnumValues
				"0 - Camera"
				"1 - CameraRoll"
				"2 - PhotoLibrary"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
