#tag Class
Protected Class PhotoMessageBoxClass
Inherits IOSMessageBox
	#tag Event
		Sub ButtonAction(buttonIndex As Integer)
		  
		  photoPicturePicker = Nil
		  photoPicturePicker = New PhotoPicturePickerClass(photoMessageBoxParent)
		  
		  Dim selection As Text = buttonIndex.ToText
		  Select Case selection
		  Case "0" //Annuler
		    //Ne rien faire
		  Case "1" // Prendre une photo
		    photoPicturePicker.Source = iOSPicturePicker.Sources.Camera
		    photoPicturePicker.Show(iosView(App.CurrentScreen.Content))
		  Case "2" // Choisir une image existante
		    photoPicturePicker.Source = iOSPicturePicker.Sources.PhotoLibrary
		    photoPicturePicker.Show(iosView(App.CurrentScreen.Content))
		  Case "3" // Détruire l'image existante
		    photoMessageBoxParent.photoParDefaut = True
		    photoMessageBoxParent.photoSelectionne = PhotoPlus
		    photoMessageBoxParent.genImagePickerClassSauveImage
		  End Select
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Constructor(parent As GenImagePickerClass)
		  photoMessageBoxParent = parent
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		photoMessageBoxParent As GenImagePickerClass
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected photoPicturePicker As PhotoPicturePickerClass
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Message"
			Visible=true
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Visible=true
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
