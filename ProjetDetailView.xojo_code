#tag IOSView
Begin iosView ProjetDetailView
   BackButtonTitle =   ""
   Compatibility   =   ""
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "#Strings.PROJETDETAIL"
   Top             =   0
   Begin iOSLabel Projet_No_Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Projet_No_Label, 1, <Parent>, 1, False, +1.00, 1, 1, 11, 
      AutoLayout      =   Projet_No_Label, 7, , 0, False, +1.00, 1, 1, 170, 
      AutoLayout      =   Projet_No_Label, 3, TopLayoutGuide, 4, False, +1.00, 1, 1, 0, 
      AutoLayout      =   Projet_No_Label, 8, , 0, False, +1.00, 1, 1, 30, 
      Enabled         =   True
      Height          =   30.0
      Left            =   11
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   2
      Text            =   "No. de projet"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   65
      Visible         =   True
      Width           =   170.0
   End
   Begin iOSLabel Projet_Titre_Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Projet_Titre_Label, 1, <Parent>, 1, False, +1.00, 1, 1, 11, 
      AutoLayout      =   Projet_Titre_Label, 2, <Parent>, 2, False, +1.00, 1, 1, 8, 
      AutoLayout      =   Projet_Titre_Label, 3, <Parent>, 3, False, +1.00, 1, 1, 101, 
      AutoLayout      =   Projet_Titre_Label, 8, , 0, False, +1.00, 1, 1, 30, 
      Enabled         =   True
      Height          =   30.0
      Left            =   11
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   2
      Text            =   "Titre"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   101
      Visible         =   True
      Width           =   317.0
   End
   Begin iOSLabel Projet_Statut_Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Projet_Statut_Label, 1, <Parent>, 1, False, +1.00, 1, 1, 11, 
      AutoLayout      =   Projet_Statut_Label, 2, <Parent>, 2, False, +1.00, 1, 1, 1, 
      AutoLayout      =   Projet_Statut_Label, 3, Projet_Titre_Label, 4, False, +1.00, 1, 1, *kStdControlGapV, 
      AutoLayout      =   Projet_Statut_Label, 8, , 0, False, +1.00, 1, 1, 30, 
      Enabled         =   True
      Height          =   30.0
      Left            =   11
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   2
      Text            =   "Statut"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   139
      Visible         =   True
      Width           =   310.0
   End
   Begin iOSLabel Projet_Charge_Projet_Nom_Label
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Projet_Charge_Projet_Nom_Label, 1, Projet_Statut_Label, 1, False, +1.00, 1, 1, 0, 
      AutoLayout      =   Projet_Charge_Projet_Nom_Label, 2, <Parent>, 2, False, +1.00, 1, 1, 174, 
      AutoLayout      =   Projet_Charge_Projet_Nom_Label, 3, Projet_Statut_Label, 4, False, +1.00, 1, 1, *kStdControlGapV, 
      AutoLayout      =   Projet_Charge_Projet_Nom_Label, 8, , 0, False, +1.00, 1, 1, 30, 
      Enabled         =   True
      Height          =   30.0
      Left            =   11
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   2
      Text            =   "Chargé de projet"
      TextAlignment   =   "0"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   177
      Visible         =   True
      Width           =   483.0
   End
   Begin Extensions.PickerViewMultiline EmployePickerView
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   EmployePickerView, 1, Projet_Charge_Projet_Nom_Label, 1, False, +1.00, 1, 1, 0, 
      AutoLayout      =   EmployePickerView, 1, Projet_Charge_Projet_Nom_Label, 1, False, +1.00, 2, 1, 0, 
      AutoLayout      =   EmployePickerView, 7, , 0, False, +1.00, 1, 1, 158, 
      AutoLayout      =   EmployePickerView, 8, , 0, False, +1.00, 1, 1, 49, 
      AutoLayout      =   EmployePickerView, 10, <Parent>, 10, False, +1.00, 1, 1, 0, 
      Height          =   49.0
      Left            =   11
      LockedInPosition=   False
      Scope           =   1
      Top             =   215
      Visible         =   True
      Width           =   158.0
   End
   Begin GIPVModule.GenImagePickerClass ImagePicker1
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   ImagePicker1, 1, EmployePickerView, 1, False, +1.00, 1, 1, 0, 
      AutoLayout      =   ImagePicker1, 7, , 0, False, +1.00, 1, 1, 94, 
      AutoLayout      =   ImagePicker1, 4, BottomLayoutGuide, 3, False, +1.00, 1, 1, -115, 
      AutoLayout      =   ImagePicker1, 3, EmployePickerView, 4, False, +1.00, 1, 1, *kStdControlGapV, 
      Height          =   92.5
      Left            =   11
      LockedInPosition=   False
      photoParDefaut  =   False
      photoSelectionne=   ""
      premiereFois    =   True
      Scope           =   1
      Top             =   272
      Visible         =   True
      Width           =   94.0
   End
   Begin iOSMessageBox AlertMessageBox1
      Left            =   0
      LeftButton      =   "Cancel"
      LockedInPosition=   False
      Message         =   ""
      PanelIndex      =   -1
      Parent          =   ""
      RightButton     =   ""
      Scope           =   2
      Title           =   ""
      Top             =   0
   End
   Begin iOSButton Button1
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Button1, 1, ImagePicker1, 1, False, +1.00, 1, 1, 0, 
      AutoLayout      =   Button1, 7, , 0, False, +1.00, 1, 1, 121, 
      AutoLayout      =   Button1, 3, <Parent>, 3, False, +1.00, 1, 1, 380, 
      AutoLayout      =   Button1, 8, , 0, False, +1.00, 1, 1, 30, 
      Caption         =   "Envoyer photos"
      Enabled         =   True
      Height          =   30.0
      Left            =   11
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   &c007AFF00
      TextFont        =   ""
      TextSize        =   0
      Top             =   380
      Visible         =   True
      Width           =   121.0
   End
   Begin ECWSSocket PDVEEWSSocket
      Left            =   0
      LockedInPosition=   False
      PanelIndex      =   -1
      Parent          =   ""
      Scope           =   0
      Top             =   0
      ValidateCertificates=   False
   End
   Begin iOSButton Button2
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Button2, 1, <Parent>, 1, False, +1.00, 1, 1, 150, 
      AutoLayout      =   Button2, 7, , 0, False, +1.00, 1, 1, 121, 
      AutoLayout      =   Button2, 10, Button1, 10, False, +1.00, 1, 1, 0, 
      AutoLayout      =   Button2, 8, , 0, False, +1.00, 1, 1, 30, 
      Caption         =   "Voir document"
      Enabled         =   True
      Height          =   30.0
      Left            =   150
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   &c007AFF00
      TextFont        =   ""
      TextSize        =   0
      Top             =   380
      Visible         =   True
      Width           =   121.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub Activate()
		  // Sélection de l'image
		  //If mProjet.projet_photo_plaque_tour <> Nil Then
		  //ImagePicker1.PhotoSelectionne = mProjet.projet_photo_plaque_tour
		  //End If
		  
		  
		  // Called when the view is displayed. This
		  // typically happens when a view that was displayed over it
		  // using PushTo is closed.
		  displayProjet(projet)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Dim button As iOSToolButton
		  
		  // Set up title bar buttons
		  button = iOSToolButton.NewPlain("Sync")
		  RightNavigationToolbar.Add(button)
		  
		  // Set up the toolbar
		  button = iOSToolButton.NewPlain(Strings.REPARATION)
		  Toolbar.Add(button)
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ToolbarPressed(button As iOSToolButton)
		  Select Case button.Caption
		    
		  Case "Sync"
		    // Synchronisation avec la BD distante
		    
		  Case Strings.REPARATION
		    // Open ReparationView
		    Dim v As New ReparationView
		    v.projet = mProjet
		    Self.BackButtonTitle = Strings.PROJETDETAIL
		    Self.PushTo(v)
		    
		  End Select
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub displayProjet(p As ProjetModule.ProjetClass)
		  If p <> Nil Then
		    Projet_No_Label.Text = p.projet_no
		    Projet_Titre_Label.Text = p.projet_titre
		    Projet_Statut_Label.Text = p.projet_statut
		    Projet_Charge_Projet_Nom_Label.Text = p.projet_charge_projet_nom
		    
		    // Affichage de l'image
		    If p.projet_photo_plaque_tour <> Nil Then
		      ImagePicker1.photoParDefaut = False
		      ImagePicker1.photoSelectionne = p.projet_photo_plaque_tour
		    Else
		      ImagePicker1.photoParDefaut = True
		    End If
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ecrireImage(imagePicker As GIPVMODULE.GenImagePickerClass)
		  
		  Dim sql As Text
		  If imagePicker.photoParDefaut Then
		    mProjet.projet_photo_plaque_tour = Nil
		    sql = "UPDATE projet SET " + _
		    "  projet_photo_plaque_tour = Null" + _
		    " WHERE projet_id = " + mProjet.projet_id.ToText
		    Try
		      App.ECWDB.SQLExecute(sql)
		    Catch er As iOSSQLiteException
		      AlertMessageBox1.Message = er.Reason
		      AlertMessageBox1.Show
		      Exit Sub
		    End Try
		  Else
		    mProjet.projet_photo_plaque_tour = imagePicker.photoSelectionne
		    Dim imgMb As MemoryBlock
		    imgMb= toDataMB(mProjet.projet_photo_plaque_tour) // my picture as IOSImage 
		    sql = "UPDATE projet SET " + _
		    "  projet_photo_plaque_tour = ?1" + _
		    " WHERE projet_id = " + mProjet.projet_id.ToText
		    Try
		      App.ECWDB.SQLExecute(sql,imgMb)
		    Catch er As iOSSQLiteException
		      AlertMessageBox1.Message = er.Reason
		      AlertMessageBox1.Show
		      Exit Sub
		    End Try
		  End If 
		  imagePicker.Invalidate
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function encodeBase64(mb as MemoryBlock) As Text
		  'From Phillip Zedalis
		  ' Modified by Michel Bujardet
		  'Function EncodeBase64(Extends MB as MemoryBlock) As Text
		  
		  Declare Function dataWithBytes Lib "UIKit" Selector "dataWithBytes:length:" (class_id As Ptr, bytes As Ptr, length As UInt32) As Ptr
		  Declare Function base64EncodedStringWithOptions Lib "UIKit" Selector "base64EncodedStringWithOptions:" (class_id As Ptr, options As UInt32) As CFStringRef
		  
		  // Create NSData pointer to be point of reference.
		  Dim data As Ptr
		  data = NSClassFromString("NSData")
		  
		  // Create NSData object using MemoryBlock
		  Dim dataRef as Ptr = dataWithBytes(data, mb.Data, mb.Size)
		  
		  // Create Text object to hold Base64 encoded string.
		  Dim x As Text
		  x = base64EncodedStringWithOptions(dataRef, 0)
		  
		  // Return Base64 encoded string
		  Return x
		  'End Function
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function toDataMB(image As iOSImage) As MemoryBlock
		  Declare function UIImageJPEGRepresentation lib UIKitLib (obj_id as ptr) as ptr
		  dim d as new Foundation.NSData(UIImageJPEGRepresentation(image.Handle))
		  
		  dim mb as MemoryBlock
		  mb = d.DataMb
		  
		  return mb
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private employe As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProjet As ProjetModule.ProjetClass
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mProjet
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProjet = value
			  
			  Self.BackButtonTitle = value.projet_no
			  
			  displayProjet(value)
			End Set
		#tag EndSetter
		projet As ProjetModule.ProjetClass
	#tag EndComputedProperty


#tag EndWindowCode

#tag Events EmployePickerView
	#tag Event
		Sub Open()
		  dim count as Integer = EmployeModule.employesArray.Ubound
		  dim nomEmployes() as Text
		  for i as Integer = 0 to count
		    nomEmployes.Append EmployeModule.employesArray(i).employe_nom_prenom
		  next
		  
		  me.AddColumn(nomEmployes)
		  me.SelectedRowInColumn(0) = 4
		  //me.SelectedRow(0) = languages.IndexOf("en-US")
		End Sub
	#tag EndEvent
	#tag Event
		Sub SelectionChanged(row as integer, column as integer)
		  employe = me.Text(row,column)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImagePicker1
	#tag Event
		Sub sauvegardeImage()
		  ecrireImage(Me)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Button1
	#tag Event
		Sub Action()
		  //Dim nomDoc As Text = "Blade Repair Report - WEC11 - Axe 1-2.pdf"
		  //Dim doc As FolderItem = SpecialFolder.Documents.Child("Ecw").Child("Documentation")..Child(nomDoc)
		  //If doc.Exists Then
		  //doc.
		  //End If
		  
		  
		  //If ImagePicker1.photoParDefaut Then
		  //Exit Sub
		  //End If 
		  
		  mProjet.projet_photo_plaque_tour = ImagePicker1.photoSelectionne
		  
		  Dim imgMb As MemoryBlock
		  imgMb= toDataMB(mProjet.projet_photo_plaque_tour) // my picture as IOSImage 
		  
		  Dim imageDonnees As New Dictionary
		  Dim sizePhoto As Integer = imgMb.Size
		  //imageDonnees.Value("SizePhoto1") = sizephoto.ToText
		  imageDonnees.Value("SizePhoto1") = sizePhoto
		  Dim json As Text
		  json = Data.GenerateJSON(imageDonnees)
		  Dim dataMb As MemoryBlock
		  dataMb = TextEncoding.UTF8.ConvertTextToData(json)
		  
		  Dim longueur As Integer = dataMb.Size
		  Dim longueur1 As Integer = imgMb.Size
		  
		  Dim mutMB as MutableMemoryBlock = New MutableMemoryBlock(1000)
		  Dim longueur2 As Integer = mutMb.Size
		  mutMB.Mid(0,dataMb.Size) = dataMb
		  longueur2 = mutMB.Size
		  mutmb.Append(imgMb)
		  longueur2 = mutMB.Size
		  
		  //Dim imgMb As MemoryBlock
		  //imgMb= toDataMB(mProjet.projet_photo_plaque_tour) // my picture as IOSImage 
		  //mutMB.Append(imgMb)
		  
		  PDVEEWSSocket.envoiImage(mutmb)
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PDVEEWSSocket
	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  
		  
		  
		  Dim jsonData As Text = TextEncoding.UTF8.ConvertDataToText(Content)
		  
		  Dim json As Dictionary
		  //json = Data.ParseJSON(jsonData)
		  //
		  //If json.Value("MessageErreur") <> "Succes" Then
		  //AlertMessageBox1.Message = json.Value("message")
		  //AlertMessageBox1.Show
		  //Exit Sub
		  //End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error(err as RuntimeException)
		  AlertMessageBox1.Message = "MessageErreur ecrire image"
		  AlertMessageBox1.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Button2
	#tag Event
		Sub Action()
		  Dim repDoc As Xojo.IO.FolderItem = SpecialFolder.Documents.Child("Ecw").Child("Documentation")
		  Dim doc As Xojo.IO.FolderItem = repDoc.Child("Blade Repair Report - WEC11 - Axe 1-2.pdf")
		  Dim urlField As Text = "file://"+ doc.Path
		  urlField = doc.URLPath
		  
		  Declare Function CFURLCreateStringByAddingPercentEscapes Lib "Foundation" (allocator As Ptr, origString As CFStringRef, charactersToLeaveUnescaped As CFStringRef, legalURLCharactersToBeEscaped As CFStringRef, encoding As UInt32) As CFStringRef
		  
		  Dim encodedURL As Text
		  encodedURL = CFURLCreateStringByAddingPercentEscapes(Nil, urlField, Nil, Nil, &h08000100)
		  
		  
		  //Dim nsURLField As NSURL = NSURL.URLWithString(encodedURL)
		  //NSURL.FileURLWithPath(encodedURL)
		  //urlField = nsURLField.URLWithString(encodedURL)
		  //urlField = "http://www.xojo.com"
		  //If doc.Exists Then
		  //If Not iOSShowUrl.ShowURL(urlField) Then
		  //AlertMessageBox1.Message = "MessageErreur visionnement document "
		  //AlertMessageBox1.Show
		  //End If
		  //End If
		  
		  //create the view controller
		  using Extensions
		  using Foundation
		  using UIKit
		  
		  //Dim controllerDocument As UIDocumentInteractionController = New UIDocumentInteractionController(NSURL.FileURLWithPath(urlField))
		  //Dim controllerDocument As UIDocumentInteractionController = New UIDocumentInteractionController(nsURLField)
		  
		  //controllerDocument.Initialize(NSURL.FileURLWithPath(urlField))
		  //controllerDocument.Initialize
		  dim controller as UIActivityViewController
		  
		  //create the controller -> create a single item array with an nsobject representing the image
		  //the second parameter is an NSArray of custom UIActivity objects, just pass nil since custom object
		  //UIActivity creation is _difficult_ in xojo
		  
		  
		  
		  //File
		  //Dim fileArr As NSArray = NSArray.CreateWithObject(NSURL.FileURLWithPath(encodedUrl))
		  Dim fileArr As NSArray = NSArray.CreateWithObject(NSURL.URLWithString(encodedUrl))
		  
		  //Second file
		  //fileArr = fileArr.ArrayByAddingObject(NSURL.FileURLWithPath(file2.Path))
		  
		  controller = new UIActivityViewController(fileArr, nil)
		  //if isIPad then
		  //controller.modalPresentationStyle = UIKit.UIViewController.UIModalPresentationStyle.popover
		  //controller.modalInPopover = true
		  //end if
		  
		  Dim excludeArr As NSArray = NSArray.CreateWithObject( new NSString(UIActivity.UIActivityTypePrint))
		  excludeArr = excludeArr.ArrayByAddingObject(new NSString(UIActivity.UIActivityTypeCopyToPasteboard))
		  excludeArr = excludeArr.ArrayByAddingObject(new NSString(UIActivity.UIActivityTypeMail))
		  controller.excludedActivityTypes = excludeArr
		  
		  //present with nil completion handler
		  self.PresentViewController(controller, True, nil)
		  
		  //if isIPad then
		  ////Then we set the popOverController position on the Toolbutton that called it
		  //Dim popController As UIKit.UIPopoverPresentationController = controller.popoverPresentationController()
		  //popController.barButtonItem = button2.Handle
		  //end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
