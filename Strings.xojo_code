#tag Module
Protected Module Strings
	#tag Constant, Name = AFAIRE, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x80 faire"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x80 faire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"To do"
	#tag EndConstant

	#tag Constant, Name = BDNONOUVERTE, Type = Text, Dynamic = True, Default = \"La base de donn\xC3\xA9es n\'a pu \xC3\xAAtre ouverte", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"La base de donn\xC3\xA9es n\'a pu \xC3\xAAtre ouverte"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La base de donn\xC3\xA9es n\'a pu \xC3\xAAtre ouverte"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Could not open database"
	#tag EndConstant

	#tag Constant, Name = CHARGEMENTENCOURS, Type = Text, Dynamic = True, Default = \"Chargement en cours...", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Chargement en cours\xE2\x80\xA6"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chargement en cours\xE2\x80\xA6"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Loading in progress\xE2\x80\xA6"
	#tag EndConstant

	#tag Constant, Name = COMPLETE, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Compl\xC3\xA9t\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Compl\xC3\xA9t\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Completed"
	#tag EndConstant

	#tag Constant, Name = CONNEXIONDISPONIBLE, Type = Text, Dynamic = True, Default = \"Connexion internet disponible. Synchronisation effectu\xC3\xA9e.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Connexion internet disponible. Synchronisation effectu\xC3\xA9e."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Connexion internet disponible. Synchronisation effectu\xC3\xA9e."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Internet connection available. Synchronisation performed."
	#tag EndConstant

	#tag Constant, Name = CONNEXIONINDISPONIBLE, Type = Text, Dynamic = True, Default = \"Connexion internet indisponible. Utilisation de la BD locale.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Connexion internet indisponible. Utilisation de la BD locale."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Connexion internet indisponible. Utilisation de la BD locale."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Internet connection unavailable. Local DB used."
	#tag EndConstant

	#tag Constant, Name = DATEDEBUT, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de d\xC3\xA9but"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de d\xC3\xA9but"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Beginning date"
	#tag EndConstant

	#tag Constant, Name = DATEFIN, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ending date"
	#tag EndConstant

	#tag Constant, Name = EMPLOYE, Type = Text, Dynamic = True, Default = \"Employ\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee"
	#tag EndConstant

	#tag Constant, Name = ENCOURS, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"En cours"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"En cours"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"In progress"
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = Text, Dynamic = True, Default = \"Erreur: ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = IMPORTATIONENCOURS, Type = Text, Dynamic = True, Default = \"Importation en cours...", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Importation en cours..."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Importation en cours..."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Importing data in progress\xE2\x80\xA6"
	#tag EndConstant

	#tag Constant, Name = NONTRAVAILLE, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Non travaill\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non travaill\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Not worked"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project"
	#tag EndConstant

	#tag Constant, Name = PROJETDETAIL, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Projet d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project detail"
	#tag EndConstant

	#tag Constant, Name = PROJETPROGRESSION, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Progression du projet"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Progression du projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project progression"
	#tag EndConstant

	#tag Constant, Name = PROJETVERSION, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ECW R\xC3\xA9paration pale (V10)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"ECW R\xC3\xA9paration pale (V10)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"ECW Blade repair (V10)"
	#tag EndConstant

	#tag Constant, Name = REPARATION, Type = Text, Dynamic = True, Default = \"R\xC3\xA9paration", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9paration"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair"
	#tag EndConstant

	#tag Constant, Name = REPARATIONDETAIL, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair detail"
	#tag EndConstant

	#tag Constant, Name = REPARATIONJTDETAIL, Type = Text, Dynamic = True, Default = \"R\xC3\xA9paration", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration jour travaill\xC3\xA9 d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9paration jour travaill\xC3\xA9 d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair working day detail"
	#tag EndConstant

	#tag Constant, Name = REPARATIONNO, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"# R\xC3\xA9paration"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"# R\xC3\xA9paration"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"# Repair"
	#tag EndConstant

	#tag Constant, Name = SYNCHRONISATIONENCOURS, Type = Text, Dynamic = True, Default = \"Synchronisation en cours...", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Synchronisation en cours\xE2\x80\xA6"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Synchronisation en cours\xE2\x80\xA6"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Synchronisation in progress\xE2\x80\xA6"
	#tag EndConstant

	#tag Constant, Name = TELECHARGEMENTDOCUMENTATION, Type = Text, Dynamic = True, Default = \"T\xC3\xA9l\xC3\xA9charg. docum. en cours...", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"T\xC3\xA9l\xC3\xA9charg. docum. en cours..."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9charg. docum. en cours..."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Downloading docum. in progress\xE2\x80\xA6"
	#tag EndConstant

	#tag Constant, Name = TRAVAILLE, Type = Text, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Travaill\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Travaill\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Worked"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
