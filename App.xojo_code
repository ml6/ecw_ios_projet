#tag Class
Protected Class App
Inherits IOSApplication
	#tag CompatibilityFlags = TargetIOS
	#tag Event
		Sub Open()
		  
		  localisation = xojo.core.Locale.Current
		  ouverture
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ouverture()
		  'Initialize and verify if on Production or Test
		  //We'll use an INI settings file to read the environment value
		  
		  //#if DebugBuild then
		  //UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  //#else
		  //UserSettings = GetFolderItem("ProdOuTest.ini")
		  //#Endif
		  
		  //UserSettings = SpecialFolder.Documents.Child("ProdOuTest.ini")
		  //OpenINI(UserSettings)
		  
		  //environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  environnementProp = "Test"
		  #if DebugBuild then
		    environnementProp = "Test"
		  #Endif
		  
		  
		  host = HOSTPROD
		  centralDatabaseName = CENTRALDATABASENAMEPROD
		  If environnementProp = "Preproduction" Then  // Nous sommes en test
		    host = HOSTPREPRODUCTION
		    centralDatabaseName = CENTRALDATABASENAMEPROD
		  ElseIf environnementProp = "Test" Then  // Nous sommes en test
		    host = HOSTTEST
		    centralDatabaseName = CENTRALDATABASENAMETEST
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		centralDatabaseName As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		ECWDB As iOSSQLiteDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		host As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h21
		Private localisation As xojo.Core.Locale
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty


	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = Text, Dynamic = True, Default = \"ecw", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMETEST, Type = Text, Dynamic = True, Default = \"ecw", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = Text, Dynamic = True, Default = \"www.ml6ecw.site", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = Text, Dynamic = True, Default = \"www.ml6ecw.site", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = Text, Dynamic = True, Default = \"192.168.0.51", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="centralDatabaseName"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="environnementProp"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="host"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
