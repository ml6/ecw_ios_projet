#tag IOSView
Begin iosView ProjetView Implements iOSTableDataSource
   BackButtonTitle =   "#Strings.PROJET"
   Compatibility   =   ""
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "#Strings.PROJETVERSION"
   Top             =   0
   Begin iOSTable ProjetsTable
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   ProjetsTable, 1, <Parent>, 1, False, +1.00, 1, 1, 0, 
      AutoLayout      =   ProjetsTable, 2, <Parent>, 2, False, +1.00, 1, 1, -0, 
      AutoLayout      =   ProjetsTable, 3, TopLayoutGuide, 3, False, +1.00, 1, 1, 0, 
      AutoLayout      =   ProjetsTable, 4, BottomLayoutGuide, 4, False, +1.00, 1, 1, 0, 
      EditingEnabled  =   False
      EditingEnabled  =   False
      EstimatedRowHeight=   -1
      Format          =   "0"
      Height          =   415.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   0
      SectionCount    =   0
      Top             =   65
      Visible         =   True
      Width           =   320.0
      Begin iOSLabel LoadingLabel
         AccessibilityHint=   ""
         AccessibilityLabel=   ""
         AutoLayout      =   LoadingLabel, 1, ProjetsTable, 1, False, +1.00, 1, 1, *kStdGapCtlToViewH, 
         AutoLayout      =   LoadingLabel, 2, LoadingProgressWheel, 1, False, +1.00, 1, 1, -*kStdControlGapH, 
         AutoLayout      =   LoadingLabel, 3, ProjetsTable, 3, False, +1.00, 1, 1, 0, 
         AutoLayout      =   LoadingLabel, 8, , 0, False, +1.00, 1, 1, 20, 
         Enabled         =   True
         Height          =   20.0
         Left            =   20
         LineBreakMode   =   "0"
         LockedInPosition=   False
         PanelIndex      =   0
         Parent          =   "ProjetsTable"
         Scope           =   2
         Text            =   "#Strings.IMPORTATIONENCOURS"
         TextAlignment   =   "0"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0
         Top             =   65
         Visible         =   False
         Width           =   231.0
      End
      Begin iOSProgressWheel LoadingProgressWheel
         AccessibilityHint=   ""
         AccessibilityLabel=   ""
         AutoLayout      =   LoadingProgressWheel, 3, LoadingLabel, 3, False, +1.00, 1, 1, 0, 
         AutoLayout      =   LoadingProgressWheel, 8, , 0, True, +1.00, 1, 1, 24, 
         AutoLayout      =   LoadingProgressWheel, 1, ProjetsTable, 1, False, +1.00, 1, 1, 259, 
         AutoLayout      =   LoadingProgressWheel, 7, , 0, True, +1.00, 1, 1, 24, 
         Height          =   24.0
         Left            =   259
         LockedInPosition=   False
         PanelIndex      =   0
         Parent          =   "ProjetsTable"
         Scope           =   2
         Shade           =   "0"
         Top             =   65
         Visible         =   True
         Width           =   24.0
      End
   End
   Begin iOSMessageBox AlertMessageBox
      Left            =   0
      LeftButton      =   "Cancel"
      LockedInPosition=   False
      Message         =   ""
      PanelIndex      =   -1
      Parent          =   ""
      RightButton     =   ""
      Scope           =   0
      Title           =   ""
      Top             =   0
   End
   Begin ECWSSocket PVEESocket
      Left            =   0
      LockedInPosition=   False
      PanelIndex      =   -1
      Parent          =   ""
      Scope           =   2
      Top             =   0
      ValidateCertificates=   False
   End
   Begin iOSMessageBox InfoMessageBox
      Left            =   0
      LeftButton      =   "OK"
      LockedInPosition=   False
      Message         =   ""
      PanelIndex      =   -1
      Parent          =   ""
      RightButton     =   ""
      Scope           =   0
      Title           =   ""
      Top             =   0
   End
   Begin ECWSSocket DocumentSocket
      Left            =   0
      LockedInPosition=   False
      PanelIndex      =   -1
      Parent          =   ""
      Scope           =   2
      Top             =   0
      ValidateCertificates=   False
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  ConnectToDB
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub connectToDB()
		  LoadingProgressWheel.Shade = iOSProgressWheel.Shades.Dark
		  LoadingProgressWheel.Visible = True
		  LoadingLabel.Visible = True
		  
		  PVEESocket.GetProjetsDonneesToutes
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function creationBdLocaleAvecTables(nomBD As Text) As Text
		  Dim message As Text = "Succes"
		  
		  Dim dbFile As FolderItem
		  dbFile = SpecialFolder.Documents.Child(nomBD)
		  
		  Dim db As New iOSSQLiteDatabase
		  db.DatabaseFile = dbFile
		  If Not db.CreateDatabaseFile Then
		    Return "La base de données n'a pu être créée."
		  End If
		  
		  // Assignation 
		  App.ECWDB = db
		  
		  message = creationTableEmploye
		  If message <> "Succes" Then Return message
		  message = creationTableReparation
		  If message <> "Succes" Then Return message
		  message = creationTableProjet
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function creationTableEmploye() As Text
		  // Création des tables
		  Dim message As Text = "Succes"
		  
		  Dim sql As Text
		  sql = "DROP TABLE IF EXISTS employe;"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  sql = "CREATE TABLE employe ( " + _
		  "  employe_id INTEGER " + _
		  ", employe_nom TEXT " + _
		  ", employe_prenom TEXT " + _
		  ", employe_nom_prenom TEXT " + _
		  ", employe_nas_last_4_digits TEXT " + _
		  ", employe_courriel  TEXT " + _
		  ", PRIMARY KEY(employe_id));"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function creationTableProjet() As Text
		  // Création des tables
		  Dim message As Text = "Succes"
		  
		  Dim sql As Text
		  sql = "DROP TABLE IF EXISTS projet;"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  sql = "CREATE TABLE projet ( " + _
		  " projet_id INTEGER " + _
		  ", projet_no TEXT " + _
		  ", projet_titre TEXT " + _
		  ", projet_statut TEXT " + _
		  ", projet_charge_projet_nom  TEXT " + _
		  ", projet_photo_plaque_tour BLOB " + _
		  ", PRIMARY KEY(projet_id));"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function creationTableReparation() As Text
		  // Création des tables
		  Dim message As Text = "Succes"
		  
		  Dim sql As Text
		  sql = "DROP TABLE IF EXISTS reparation;"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  sql = "CREATE TABLE reparation ( " + _
		  " reparation_no TEXT " + _
		  ", reparation_date_debut TEXT " + _
		  ", reparation_projet_no TEXT " + _
		  ", reparation_progression_statut Text " + _
		  ", PRIMARY KEY(reparation_projet_no,reparation_no,reparation_date_debut));"
		  Try
		    App.ECWDB.SQLExecute(sql) 
		  Catch e As iOSSQLiteException
		    Return e.Reason
		  End Try  
		  
		  Return message
		  
		  //sql = "CREATE TABLE reparation ( " + _
		  //" reparation_no TEXT " + _
		  //", reparation_date_debut TEXT " + _
		  //", reparation_projet_no TEXT " + _
		  //", reparation_travaille Text " + _
		  //", reparation_photo_01 BLOB " + _
		  //", reparation_photodesc_01 TEXT " + _
		  //", reparation_photo_02 BLOB " + _
		  //", reparation_photodesc_02 TEXT " + _
		  //", reparation_photo_03 BLOB " + _
		  //", reparation_photodesc_03 TEXT " + _
		  //", reparation_photo_04 BLOB " + _
		  //", reparation_photodesc_04 TEXT " + _
		  //", reparation_photo_05 BLOB " + _
		  //", reparation_photodesc_05 TEXT " + _
		  //", reparation_photo_06 BLOB " + _
		  //", reparation_photodesc_06 TEXT " + _
		  //", reparation_photo_07 BLOB " + _
		  //", reparation_photodesc_07 TEXT " + _
		  //", reparation_photo_08 BLOB " + _
		  //", reparation_photodesc_08 TEXT " + _
		  //", reparation_photo_09 BLOB " + _
		  //", reparation_photodesc_09 TEXT " + _
		  //", reparation_photo_10 BLOB " + _
		  //", reparation_photodesc_10 TEXT " + _
		  //", PRIMARY KEY(reparation_projet_no,reparation_no,reparation_date_debut));"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub majEmployesBdLocale()
		  
		  Dim sql As Text
		  sql = "SELECT * FROM employe "
		  Dim employeRS As iOSSQLiteRecordSet
		  Try
		    employeRS = App.ECWDB.SQLSelect(sql)
		  Catch er As iOSSQLiteException
		    AlertMessageBox.Message = er.Reason
		    AlertMessageBox.Show
		    Exit Sub
		  End Try
		  
		  ReDim employesArray(-1)
		  If employeRS <> Nil Then
		    While Not employeRS.EOF
		      
		      Dim e As New EmployeModule.EmployeClass
		      e.employe_id = employeRS.Field("employe_id").IntegerValue
		      e.employe_nom = employeRS.Field("employe_nom").TextValue
		      e.employe_prenom = employeRS.Field("employe_prenom").TextValue
		      e.employe_nom_prenom = employeRS.Field("employe_nom").TextValue + " " + employeRS.Field("employe_prenom").TextValue
		      e.employe_nas_last_4_digits = employeRS.Field("employe_nas_last_4_digits").TextValue
		      e.employe_courriel = employeRS.Field("employe_courriel").TextValue
		      
		      // Add employe
		      employesArray.Append(e)
		      employeRS.MoveNext
		    Wend
		    employeRS.Close
		  End If
		  employeRS = Nil
		  
		  // Sort Employees
		  Dim sortEmployes() As Text
		  For Each s As EmployeModule.EmployeClass In employesArray
		    sortEmployes.Append(s.employe_nom + " " + s.employe_prenom)
		  Next
		  sortEmployes.SortWith(employesArray)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub majProjetsBdLocale()
		  //Self.Title = "Projets"
		  Self.TabTitle = ""
		  
		  Dim sql As Text
		  sql = "SELECT * FROM projet "
		  Dim projetRS As iOSSQLiteRecordSet
		  Try
		    projetRS = App.ECWDB.SQLSelect(sql)
		  Catch er As iOSSQLiteException
		    AlertMessageBox.Message = er.Reason
		    AlertMessageBox.Show
		    Exit Sub
		  End Try
		  
		  ReDim sectionsArray(-1)
		  If projetRS <> Nil Then
		    While Not projetRS.EOF
		      
		      Dim p As New ProjetModule.ProjetClass
		      p.projet_id = projetRS.Field("projet_id").IntegerValue
		      p.projet_no = projetRS.Field("projet_no").TextValue
		      p.projet_titre = projetRS.Field("projet_titre").TextValue
		      p.projet_statut = projetRS.Field("projet_statut").TextValue
		      p.projet_charge_projet_nom = projetRS.Field("projet_charge_projet_nom").TextValue
		      If projetRS.Field("projet_photo_plaque_tour").NativeValue <> Nil Then
		        p.projet_photo_plaque_tour = iOSImage.FromData(projetRS.Field("projet_photo_plaque_tour").NativeValue)
		      End If
		      // Add to the appropriate section
		      Dim addedToSection As Boolean
		      For Each s As ProjetModule.SectionClass In sectionsArray
		        If s.title = p.projet_statut Then
		          s.projets.Append(p)
		          addedToSection = True
		          Exit For
		        End If
		      Next
		      
		      If Not addedToSection Then
		        Dim s As New ProjetModule.SectionClass
		        s.Title = p.projet_statut 
		        sectionsArray.Append(s)
		        s.projets.Append(p)
		      End If
		      
		      // Add projet
		      projetRS.MoveNext
		    Wend
		    
		    projetRS.Close
		  End If
		  projetRS = Nil
		  
		  // Sort sections
		  Dim sortSections() As Text
		  For Each s As ProjetModule.SectionClass In sectionsArray
		    sortSections.Append(s.title)
		  Next
		  sortSections.SortWith(sectionsArray)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RowCount(table as iOSTable, section As Integer) As Integer
		  // Part of the iOSTableDataSource interface.
		  
		  Return sectionsArray(section).Projets.Ubound + 1
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RowData(table as iOSTable, section As Integer, row As Integer) As iOSTableCellData
		  // Part of the iOSTableDataSource interface.
		  
		  Dim cell As iOSTableCellData
		  cell = ProjetsTable.CreateCell
		  Dim p As ProjetModule.ProjetClass = sectionsArray(section).Projets(row)
		  
		  cell.Text = p.projet_no
		  cell.DetailText = p.projet_titre
		  cell.AccessoryType = iOSTableCellData.AccessoryTypes.Disclosure
		  cell.Tag = p
		  
		  Return cell
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SectionCount(table as iOSTable) As Integer
		  // Part of the iOSTableDataSource interface.
		  
		  Return sectionsArray.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SectionTitle(table as iOSTable, section As Integer) As Text
		  // Part of the iOSTableDataSource interface.
		  
		  Return sectionsArray(section).title
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub syncEmployes(json As Dictionary)
		  
		  Dim employesDict As Dictionary
		  employesDict = json.Value("GetEmployesTous")
		  
		  Dim sql As Text
		  sql = "DELETE FROM employe"
		  Try
		    App.ECWDB.SQLExecute(sql)
		  Catch er As iOSSQLiteException
		    AlertMessageBox.Message = er.Reason
		    AlertMessageBox.Show
		    Exit Sub
		  End Try
		  
		  ReDim EmployeModule.employesArray(-1)
		  For Each entry As DictionaryEntry In employesDict
		    Dim e As New EmployeModule.EmployeClass
		    e.employe_id = Integer.FromText(entry.Key)
		    Dim emplDict As Dictionary = entry.Value
		    e.employe_nom = emplDict.Value("employe_nom")
		    e.employe_prenom = emplDict.Value("employe_prenom")
		    e.employe_nom_prenom = emplDict.Value("employe_nom") + " " + emplDict.Value("employe_prenom")
		    e.employe_nas_last_4_digits = emplDict.Value("employe_nas_last_4_digits")
		    e.employe_courriel = emplDict.Value("employe_courriel")
		    
		    // Add employe
		    EmployeModule.employesArray.Append(e)
		    
		    // Maj de la table employe
		    sql = "INSERT INTO employe (employe_id, employe_nom, employe_prenom, employe_nom_prenom, " + _
		    "employe_nas_last_4_digits, employe_courriel) " + _
		    "VALUES( " + entry.Key + _
		    ", '" + e.employe_nom.ReplaceAll("'","''") + "' " + _
		    ", '" + e.employe_prenom.ReplaceAll("'","''") + "' " + _
		    ", '" + e.employe_nom_prenom.ReplaceAll("'","''") + "' " + _
		    ", '" + e.employe_nas_last_4_digits + "' " + _
		    ", '" + e.employe_courriel + "' " + _
		    ")"
		    Try
		      App.ECWDB.SQLExecute(sql)
		    Catch er As iOSSQLiteException
		      AlertMessageBox.Message = er.Reason
		      AlertMessageBox.Show
		      Exit Sub
		    End Try
		    
		  Next
		  
		  // Sort Employees
		  Dim sortEmployes() As Text
		  For Each s As EmployeModule.EmployeClass In EmployeModule.employesArray
		    sortEmployes.Append(s.employe_nom + " " + s.employe_prenom)
		  Next
		  sortEmployes.SortWith(EmployeModule.employesArray)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub synchronisationBdDistanteBDLocale(json As Dictionary)
		  Dim message As Text = ""
		  
		  // Vérification si la bd locale existe
		  Dim dbFile As FolderItem
		  dbFile = SpecialFolder.Documents.Child("ECW_Projet.sqlite")
		  //dbFile.Delete
		  
		  // Si la bd locale n'existe pas -> Création
		  If Not dbFile.Exists Then
		    message = creationBdLocaleAvecTables("ECW_Projet.sqlite")
		    If message <> "Succes" Then
		      AlertMessageBox.Message = message
		      AlertMessageBox.Show
		      Exit Sub
		    End If
		  Else
		    App.ECWDB = New iOSSQLiteDatabase
		    App.ECWDB.DatabaseFile = dbFile
		  End If
		  
		  If Not App.ECWDB.Connect Then
		    AlertMessageBox.Message = Strings.BDNONOUVERTE
		    AlertMessageBox.Show
		    Exit Sub
		  End If
		  
		  If json.HasKey("GetEmployesTous") Then
		    syncEmployes(json)
		  End If
		  
		  If json.HasKey("GetReparationsTous") Then
		    syncReparations(json)
		  End If
		  
		  If json.HasKey("GetProjetsTous") Then
		    syncProjets(json)
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub syncProjets(json As Dictionary)
		  //Self.Title = "Projets"
		  Self.TabTitle = ""
		  
		  Dim projetsDict As Dictionary
		  projetsDict = json.Value("GetProjetsTous")
		  
		  ReDim sectionsArray(-1)
		  
		  For Each entry As DictionaryEntry In projetsDict
		    Dim p As New ProjetModule.ProjetClass
		    p.projet_id = Integer.FromText(entry.Key)
		    
		    Dim projDict As Dictionary = entry.Value
		    
		    p.projet_no = projDict.Value("projet_no")
		    p.projet_titre = projDict.Value("projet_titre")
		    p.projet_statut = projDict.Value("projet_statut")
		    p.projet_charge_projet_nom = projDict.Value("projet_charge_projet_nom")
		    
		    // Mise-à-jour de la base de données locale
		    Dim sql As Text
		    sql = "SELECT count(*) FROM projet WHERE projet_id = " + entry.Key
		    Dim projetRS As iOSSQLiteRecordSet
		    Try
		      projetRS = App.ECWDB.SQLSelect(sql)
		    Catch er As iOSSQLiteException
		      AlertMessageBox.Message = er.Reason
		      AlertMessageBox.Show
		      Exit Sub
		    End Try
		    
		    Dim nbEnreg As Integer = 0
		    If projetRS <> Nil Then
		      nbEnreg = projetRS.IdxField(0).IntegerValue
		      projetRS.Close
		    End If
		    If nbEnreg = 1 Then // Si existant, Maj
		      sql = "UPDATE projet SET " + _
		      "  projet_no = '" + p.projet_no + "' " + _
		      ", projet_titre = '" + p.projet_titre.ReplaceAll("'","''") + "' " + _
		      ", projet_statut = '" + p.projet_statut + "' " + _
		      ", projet_charge_projet_nom = '" + p.projet_charge_projet_nom.ReplaceAll("'","''") + "' " + _
		      " WHERE projet_id = " + entry.Key
		      Try
		        App.ECWDB.SQLExecute(sql)
		      Catch er As iOSSQLiteException
		        AlertMessageBox.Message = er.Reason
		        AlertMessageBox.Show
		        Exit Sub
		      End Try
		    Else //  Sinon, l'enregistrement doit être ajouté
		      sql = "INSERT INTO projet (projet_id, projet_no, projet_titre, projet_statut, " + _
		      "projet_charge_projet_nom) " + _
		      "VALUES( " + entry.Key + _
		      ", '" + p.projet_no + "' " + _
		      ", '" + p.projet_titre.ReplaceAll("'","''") + "' " + _
		      ", '" + p.projet_statut + "' " + _
		      ", '" + p.projet_charge_projet_nom.ReplaceAll("'","''") + "' " + _
		      ")"
		      Try
		        App.ECWDB.SQLExecute(sql)
		      Catch er As iOSSQLiteException
		        AlertMessageBox.Message = er.Reason
		        AlertMessageBox.Show
		        Exit Sub
		      End Try
		    End If
		    
		    // Add to the appropriate section
		    Dim addedToSection As Boolean
		    For Each s As ProjetModule.SectionClass In sectionsArray
		      If s.title = p.projet_statut Then
		        s.projets.Append(p)
		        addedToSection = True
		        Exit For
		      End If
		    Next
		    
		    If Not addedToSection Then
		      Dim s As New ProjetModule.SectionClass
		      s.Title = p.projet_statut 
		      sectionsArray.Append(s)
		      s.projets.Append(p)
		    End If
		  Next
		  
		  // Sort sections
		  Dim sortSections() As Text
		  For Each s As ProjetModule.SectionClass In sectionsArray
		    sortSections.Append(s.title)
		  Next
		  sortSections.SortWith(sectionsArray)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub syncReparations(json As Dictionary)
		  
		  Dim reparationsDict As Dictionary
		  reparationsDict = json.Value("GetReparationsTous")
		  
		  Dim sql As Text
		  sql = "DELETE FROM reparation"
		  Try
		    App.ECWDB.SQLExecute(sql)
		  Catch er As iOSSQLiteException
		    AlertMessageBox.Message = er.Reason
		    AlertMessageBox.Show
		    Exit Sub
		  End Try
		  
		  ReDim ReparationModule.reparationsArray(-1)
		  For Each entry As DictionaryEntry In reparationsDict
		    Dim r As New ReparationModule.ReparationClass
		    Dim emplDict As Dictionary = entry.Value
		    r.reparation_projet_no = emplDict.Value("reparation_projet_no")
		    r.reparation_no = emplDict.Value("reparation_no")
		    r.reparation_date_debut = emplDict.Value("reparation_date_debut")
		    r.reparation_progression_statut = Strings.AFAIRE
		    If emplDict.Value("reparation_progression_statut") = "2" Then r.reparation_progression_statut = Strings.ENCOURS
		    If emplDict.Value("reparation_progression_statut") = "3" Then r.reparation_progression_statut = Strings.COMPLETE
		    
		    // Add reparation
		    ReparationModule.reparationsArray.Append(r)
		    
		    // Maj de la table reparation
		    sql = "INSERT INTO reparation (reparation_projet_no, reparation_no, reparation_date_debut, " + _
		    "reparation_progression_statut) " + _
		    "VALUES( '" + r.reparation_projet_no + "' " + _
		    ", '" + r.reparation_no + "' " + _
		    ", '" + r.reparation_date_debut + "' " + _
		    ", '" + emplDict.Value("reparation_progression_statut") + "' " + _
		    ")"
		    Try
		      App.ECWDB.SQLExecute(sql)
		    Catch er As iOSSQLiteException
		      AlertMessageBox.Message = er.Reason
		      AlertMessageBox.Show
		      Exit Sub
		    End Try
		    
		  Next
		  
		  // Sort Reparations
		  Dim sortReparations() As Text
		  For Each r As ReparationModule.ReparationClass In ReparationModule.reparationsArray
		    sortReparations.Append(r.reparation_no + " " + r.reparation_date_debut)
		  Next
		  sortReparations.SortWith(ReparationModule.reparationsArray)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub telechargeDocumentation(t As Timer)
		  #Pragma Unused t
		  
		  If telechargementDocumentFin Then
		    mCounter = mCounter + 1
		    LoadingLabel.Text = Strings.TELECHARGEMENTDOCUMENTATION + " " + mCounter.ToText
		    telechargementDocumentDebut = True
		    telechargementDocumentFin = False
		  End If
		  
		  If telechargementDocumentDebut Then
		    telechargementDocumentIndex = telechargementDocumentIndex + 1
		    If telechargementDocumentIndex <= docArray.Ubound Then
		      telechargementDocumentDebut = False
		      telechargementDocumentEnCours = True
		      
		      Dim tmpRep As Xojo.IO.FolderItem
		      tmpRep = Xojo.IO.SpecialFolder.Temporary
		      
		      Dim name As Text
		      Dim urlField As Text
		      urlField = docArray(telechargementDocumentIndex)
		      Dim arrayURL(-1) As Text
		      arrayURL = urlField.Split("/")
		      name = arrayURL(arrayURL.Ubound)
		      
		      Dim outputFile As Xojo.IO.FolderItem = tmpRep.Child(name)
		      If outputFile.Exists Then 
		        outputFile.Delete
		      End If
		      
		      DocumentSocket.ClearRequestHeaders
		      DocumentSocket.GetDocument(urlField, outputFile)
		    Else
		      mTimer.Mode = Timer.Modes.Off
		      // Because the View implements iOSTableDataSource, the view
		      // can automatically provide the data to display in the table
		      // using the TableRow and TableRowCount methods.
		      ProjetsTable.DataSource = Self
		      Exit Sub
		    End If
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub utilisationBdLocale()
		  Dim message As Text = ""
		  
		  // Vérification si la bd locale existe
		  Dim dbFile As FolderItem
		  dbFile = SpecialFolder.Documents.Child("ECW_Projet.sqlite")
		  
		  // Si la bd locale n'existe pas -> Création
		  If Not dbFile.Exists Then
		    message = creationBdLocaleAvecTables("ECW_Projet.sqlite")
		    If message <> "Succes" Then
		      AlertMessageBox.Message = message
		      AlertMessageBox.Show
		      Exit Sub
		    End If
		  Else
		    App.ECWDB = New iOSSQLiteDatabase
		    App.ECWDB.DatabaseFile = dbFile
		  End If
		  
		  If Not App.ECWDB.Connect Then
		    AlertMessageBox.Message = "La base de données n'a pu être ouverte."
		    AlertMessageBox.Show
		    Exit Sub
		  End If
		  
		  // Chargement à partir de la BD locale
		  
		  // Chargement table employe
		  majEmployesBdLocale
		  
		  // Chargement table Projet
		  majProjetsBdLocale
		  
		  // Because the View implements iOSTableDataSource, the view
		  // can automatically provide the data to display in the table
		  // using the TableRow and TableRowCount methods.
		  ProjetsTable.DataSource = Self
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private currentProjet As ProjetModule.ProjetClass
	#tag EndProperty

	#tag Property, Flags = &h21
		Private docArray() As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCounter As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTimer As Timer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sectionsArray() As ProjetModule.SectionClass
	#tag EndProperty

	#tag Property, Flags = &h21
		Private telechargementDocumentDebut As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private telechargementDocumentEnCours As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private telechargementDocumentFin As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private telechargementDocumentIndex As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private telechargementTouteDocumentationTermine As Boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events ProjetsTable
	#tag Event
		Sub Action(section As Integer, row As Integer)
		  Dim details As New ProjetDetailView
		  details.projet = ProjetModule.ProjetClass(Me.RowData(section, row).Tag)
		  
		  If Self.ParentSplitView <> Nil Then
		    // iPad
		    // Selecting a customer should display its details in the detail
		    // view of the split.
		    Self.ParentSplitView.Detail = details
		  Else
		    // iPhone
		    Self.PushTo(details)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PVEESocket
	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  // Requested data was received back from web service
		  
		  Dim jsonData As Text = TextEncoding.UTF8.ConvertDataToText(Content)
		  
		  Dim json As Dictionary
		  json = Data.ParseJSON(jsonData)
		  
		  If json.Value("MessageErreur") <> "Succes" Then
		    AlertMessageBox.Message = json.Value("MessageErreur")
		    AlertMessageBox.Show
		    Exit Sub
		  End If
		  
		  // La BD distante a été jointe, synchronisation avec la bd locale
		  synchronisationBdDistanteBDLocale(json)
		  
		  ReDim docArray(-1)
		  docArray.Append("http://192.168.0.51/ecw/gestproj/documentation/Blade Repair Report - WEC11 - Axe 1-2.pdf")
		  docArray.Append("http://192.168.0.51/ecw/gestproj/documentation/DIGBY BLADE REPAIR T1-B1 2014-09-10.pdf")
		  
		  If docArray.Ubound > 0 Then
		    // Déclenchement du Timer pour le téléchargement de la documentation
		    
		    // Création du répertoire de la documentation pour les réparations, s'il y a lieu
		    Dim repDoc As FolderItem = SpecialFolder.Documents.Child("Ecw")
		    If Not repDoc.Exists Then 
		      repDoc.CreateAsFolder
		      repDoc = repdoc.Child("Documentation")
		      repDoc.CreateAsFolder
		    End If
		    
		    mTimer = New Timer
		    mTimer.Mode = Timer.Modes.Multiple
		    mTimer.Period = 500
		    telechargementDocumentDebut = True
		    telechargementTouteDocumentationTermine = False
		    telechargementDocumentIndex = -1
		    AddHandler mTimer.Action, AddressOf telechargeDocumentation
		  Else
		    // Because the View implements iOSTableDataSource, the view
		    // can automatically provide the data to display in the table
		    // using the TableRow and TableRowCount methods.
		    ProjetsTable.DataSource = Self
		  End If
		  
		  //LoadingLabel.Visible = False
		  //LoadingProgressWheel.Visible = False
		  //
		  //InfoMessageBox.Message = Strings.CONNEXIONDISPONIBLE
		  //InfoMessageBox.Show
		  
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error(err as RuntimeException)
		  
		  LoadingLabel.Visible = False
		  LoadingProgressWheel.Visible = False
		  
		  InfoMessageBox.Message = err.ErrorNumber.ToText + ": " + err.Reason + " " + Strings.CONNEXIONINDISPONIBLE
		  InfoMessageBox.Show
		  
		  // Si la BD distante n'est pas joignable, chargement de la bd locale sans synchronisation
		  utilisationBdLocale
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DocumentSocket
	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  #Pragma Unused URL
		  #Pragma Unused httpStatus
		  #Pragma Unused Content
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error(err as RuntimeException)
		  
		  LoadingLabel.Visible = False
		  LoadingProgressWheel.Visible = False
		  
		  InfoMessageBox.Message = err.ErrorNumber.ToText + ": " + err.Reason + " " + Strings.CONNEXIONINDISPONIBLE
		  InfoMessageBox.Show
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub FileReceived(URL as Text, HTTPStatus as Integer, File as xojo.IO.FolderItem)
		  #Pragma Unused URL
		  #Pragma Unused httpStatus
		  
		  Dim repDoc As Xojo.IO.FolderItem = SpecialFolder.Documents.Child("Ecw").Child("Documentation")
		  Dim doc As Xojo.IO.FolderItem = repDoc.Child(File.Name)
		  If doc.Exists Then 
		    doc.Delete
		  End If
		  File.CopyTo(repDoc)
		  // Delete document
		  File.Delete
		  
		  telechargementDocumentFin = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
