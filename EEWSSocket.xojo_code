#tag Class
Protected Class EEWSSocket
Inherits Xojo.Net.HTTPSocket
	#tag Method, Flags = &h0
		Sub envoiImage(image As MemoryBlock)
		  
		  Self.SetRequestContent(image, "application/x-www-form-urlencoded")
		  Self.Send("POST", "http://192.168.0.51:7060/special/ecrireImage")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetProjetsDonneesToutes()
		  // Call the WebService to get projects
		  Self.Send("POST", "http://192.168.0.51:7060/special/getProjetsDonneesToutes")
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ValidateCertificates"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
