#tag Class
Protected Class ProjetClass
	#tag Property, Flags = &h0
		projet_charge_projet As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_charge_projet_nom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_coordonnees As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_nom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_site_adresse As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_site_contact As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_debut_prevue As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_debut_reelle As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_fin_prevue As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_fin_reelle As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_estimateur As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_estimateur_nom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_no As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_photo_chemin As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_photo_plaque_tour As iOSImage
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_statut As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_titre As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_type As Text
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_charge_projet"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_charge_projet_nom"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_coordonnees"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_nom"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_site_adresse"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_site_contact"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_debut_prevue"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_debut_reelle"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_fin_prevue"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_fin_reelle"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_estimateur"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_estimateur_nom"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_no"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_photo_chemin"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_photo_plaque_tour"
			Group="Behavior"
			Type="iOSImage"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_statut"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_titre"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_type"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
