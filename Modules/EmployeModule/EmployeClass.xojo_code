#tag Class
Protected Class EmployeClass
	#tag Property, Flags = &h0
		employe_code_postal As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_compte As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_courriel As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_nas_last_4_digits As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_nom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_nom_prenom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_no_civique As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_prenom As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_province As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_rue As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_statut As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tdem As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tdou As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tel As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_transit As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_treg As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_ville As Text
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="employe_code_postal"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_compte"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_courriel"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_nas_last_4_digits"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_nom"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_nom_prenom"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_no_civique"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_prenom"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_province"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_rue"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_statut"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tdem"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tdou"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tel"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_transit"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_treg"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_ville"
			Group="Behavior"
			Type="Text"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
